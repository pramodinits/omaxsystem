<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
<!--        <meta name="viewport" content="width=device-width">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link rel="shortcut icon" href="<?php echo esc_url(get_template_directory_uri()); ?>/img/favicon.ico" type="image/x-icon">

        <!---Custom css-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/full-slider.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/jquery.simplyscroll.css" type="text/css" />

        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.simplyscroll.js" type="text/javascript"></script>
        <script type="text/javascript">
            (function ($) {
                $(function () {
                    $("#scroller").simplyScroll();
                });
            })(jQuery);
        </script>

        <link rel="stylesheet" type="text/css" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/main.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/animations.css" />
        <script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/slider.js"></script>
        <!---Custom css-->
        <script type="text/javascript">
            $(function () {
//                $('ul li a[href^="' + location.pathname.split("/")[1] + '"]').addClass("active-color");
                $('ul li ul li a[href^="' + location.pathname.split("/")[1] + '"]').addClass("active");
            });
            
           jQuery(function($) {
 var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
 $('ul.dropdown-menu a').each(function() {
  if (this.href === path) {
   $(this).addClass('active');
  }
 });
});
        </script>
        <script type="text/javascript">
            function emailsendcontact() {
                var subject = "Contact Us";
                var fromname = $('#fromName').val();
                var company = $('#company').val();
                var fromAddress = $('#fromAddress').val();
                var city = $('#city').val();
                var zipCode = $('#zipCode').val();
                var country = $('#country').val();
                var price = $('#price').val();
                var lastName = $('#lastName').val();
                var fromEmail = $('#fromEmail').val();
                var state = $('#state').val();
                var fromPhone = $('#fromPhone').val();
                var need = $('#need').val();
                var fromMessage = $('#fromMessage').val();
                $.post("../mailScriptContact.php",
                        {"subject": subject, "fromname": fromname, "company": company, "fromAddress": fromAddress, "city": city, "zipCode": zipCode, "country": country,
                            "price": price, "lastName": lastName, "fromEmail": fromEmail, "state": state, "fromPhone": fromPhone, "need": need, "fromMessage": fromMessage},
                function (result) {
    console.log(result);
                    $('#contactForm').each(function () {
                        this.reset();
                    });
                    $(".alert-success").show();
                    setTimeout(function() { $(".alert-success").fadeOut(1500);}, 5000)
                });
            }
            
            function emailsendemployee() {
                var subject = "Employers";
                var fromname = $('#fromName').val();
                var company = $('#company').val();
                var fromAddress = $('#fromAddress').val();
                var city = $('#city').val();
                var zipCode = $('#zipCode').val();
                var country = $('#country').val();
                var price = $('#price').val();
                var lastName = $('#lastName').val();
                var fromEmail = $('#fromEmail').val();
                var state = $('#state').val();
                var fromPhone = $('#fromPhone').val();
                var need = $('#need').val();
                var fromMessage = $('#fromMessage').val();
                $.post("../mailScriptEmployee.php",
                        {"subject": subject, "fromname": fromname, "company": company, "fromAddress": fromAddress, "city": city, "zipCode": zipCode, "country": country,
                            "price": price, "lastName": lastName, "fromEmail": fromEmail, "state": state, "fromPhone": fromPhone, "need": need, "fromMessage": fromMessage},
                function (result) {
    console.log(result);
                    $('#employeeForm').each(function () {
                        this.reset();
                    });
                    $(".alert-success").show();
                    setTimeout(function() { $(".alert-success").fadeOut(1500);}, 5000)
                });
            }

        </script>    
    </head>

    <body>
        <div id="page" class="hfeed site">
            <div id="content" class="site-content">
