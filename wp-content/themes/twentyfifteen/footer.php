<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer>
            <section class="co-name">
                <div class="container">
                    <div class="row footer_container">
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-services_head">Pages</h3>
                            <hr>
                            <ul class="footer-services device-w">
                                <li><a href="<?php echo home_url(); ?>/industry-expertise/" title="">Industry Expertise</a></li>
                                <li><a href="#" title="">IT Services</a></li>
                                <li><a href="<?php echo home_url(); ?>/find-jobs/" title="">Find Jobs</a></li>
                                <li><a href="<?php echo home_url(); ?>/employers/" title="">Employers</a></li>
                                <li><a href="<?php echo home_url(); ?>/contact-us/" title="">Contact Us</a></li>
                                <li><a href="#" title="">Privacy Policy</a></li>
                                <li><a href="#" title="">Legal Notice</a></li>
                                <li><a href="<?php echo home_url(); ?>/contact-us/" title="">Get a Quote</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-services_head">Industry Expertise</h3>
                            <hr>
                            <ul class="footer-services device-w">
                                <li><a href="<?php echo home_url(); ?>/industry-expertise/#finance" title="Financial &amp; Services">Financial &amp; Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#insurance" title="Insurance">Insurance</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#pharmaceuticals" title="Pharmaceuticals">Pharmaceuticals</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#ecommerce" title="E-Commerce">E-Commerce</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#healthcare" title="Health &amp; Care">Health Care</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#media" title="Media">Media</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#telecom" title="Telecom">Telecom</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#hospitality" title="Hospitality">Hospitality</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#retail" title="Retail">Retail</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-services_head">IT Services</h3>
                            <hr>
                            <ul class="footer-services device-w">
                                <li><a href="<?php echo home_url(); ?>/big-data-analytics/" title="Big Data & Analytics">Big Data & Analytics</a></li>
                                <li><a href="<?php echo home_url(); ?>/cloud-services/" title="Cloud Services">Cloud Services</a></li>
                                <li><a href="<?php echo home_url(); ?>/it-consulting/" title="IT Consulting">IT Consulting</a></li>
                                <li><a href="<?php echo home_url(); ?>/software-app-development/" title="Software & App Development">Software & App Development</a></li>
                                <li><a href="<?php echo home_url(); ?>/testing-services/" title="Testing Services">Testing Services</a></li>
                                <li><a href="<?php echo home_url(); ?>/it-staffing/" title="IT Staffing">IT Staffing</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6 footer-bottom-info">
                            <h3 class="footer-services_head">Contact Info</h3>
                            <hr>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/map.png"><span>PO Box 1879 Media, PA 19063</span><br>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/call.png"><span>610-707-2244</span><br>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mail.png"><span>info@solutionprimary.com</span>
                        </div>
                    </div>
                    <div class="row copyright-text">
                        &copy; 2017 - SOLUTION PRIMARY - All Rights Reserved
                    </div>
                </div>
            </section>
        </footer>

</div><!-- .site -->

<?php wp_footer(); ?>

</body>

<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            var sticky = $('.head'),
                scroll = $(window).scrollTop();
            if (scroll >= 100) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });

    });
</script>

</html>
