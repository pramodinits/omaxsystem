<?php
/* Template Name: Home Page Template */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<title>Solution Primary</title>
<?php get_header(); ?>
<style>
 .head {
            position: fixed;
            z-index: 99999;
            width: 100%;
            margin: 0px;
            padding: 0px;
            top: 0px;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 900px) {
            .head {
                position: relative;
            }
        }
        /*end*/
        
        #txtName:focus {
            border: 1px solid red;
        }

        #txtEmail:focus {
            border: 1px solid red;
        }

        #txtPhone:focus {
            border: 1px solid red;
        }

        #txtMsg:focus {
            border: 1px solid red;
        }
        #myCarousel p {
            display: none;
        }
   
</style>
<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <div class="container-fluid head">
            <header class="top_bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 logo">
                            <a href="<?php echo home_url(); ?>" title="">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo_MP.png" alt=""/>
                            </a>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3 col-sm-6 toll-free">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/call_header.png"><span>&nbsp;&nbsp;610 707 2244</span>
                            <br/>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mail_header.png"><span>&nbsp;&nbsp;info@solutionprimary.com</span>
                        </div>
                    </div>
                </div>
            </header>
            <nav class="navbar navbar-inverse navbar_ed">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul id="nav" class="nav navbar-nav">
                            <li class="active"><a href="#" title="Home">HOME</a></li>
                            <li class="dropdown">
                                <a href="<?php echo home_url(); ?>/industry-expertise/" class="dropdown-toggle hidden-xs" aria-haspopup="true" aria-expanded="false">INDUSTRY EXPERTISE</a>
                                <a href="#" class="dropdown-toggle hidden-lg hidden-md hidden-sm visible-xs" aria-haspopup="true" aria-expanded="false">INDUSTRY EXPERTISE</a>
                                <ul class="dropdown-menu">
                                    <li class="hidden-lg hidden-md hidden-sm visible-xs"><a href="<?php echo home_url(); ?>/industry-expertise/" title="Industry &amp; Expertise">Industry Expertise</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#finance" title="Financial &amp; Services">Financial &amp; Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#insurance" title="Insurance">Insurance</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#pharmaceuticals" title="Pharmaceuticals">Pharmaceuticals</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#ecommerce" title="E-Commerce">E-Commerce</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#healthcare" title="Health &amp; Care">Health Care</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#media" title="Media">Media</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#telecom" title="Telecom">Telecom</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#hospitality" title="Hospitality">Hospitality</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#retail" title="Retail">Retail</a></li>
                                </ul>
                            </li>
                            <li id="service" class="dropdown"><a href="#" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">IT SERVICES</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo home_url(); ?>/big-data-analytics/" title="Big Data & Analytics">Big Data & Analytics</a></li>
                                    <li><a href="<?php echo home_url(); ?>/cloud-services/" title="Cloud Services">Cloud Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/it-consulting/" title="IT Consulting">IT Consulting</a></li>
                                    <li><a href="<?php echo home_url(); ?>/software-app-development/" title="Software & App Development">Software & App Development</a></li>
                                    <li><a href="<?php echo home_url(); ?>/testing-services/" title="Testing Services">Testing Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/it-staffing/" title="IT Staffing">IT Staffing</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo home_url(); ?>/find-jobs/" title="FIND JOBS">FIND JOBS</a></li>
                            <li><a href="<?php echo home_url(); ?>/employers/" title="EMPLOYERS">EMPLOYERS</a></li>
                            <li><a href="<?php echo home_url(); ?>/contact-us/" title="CONTACT US">CONTACT US</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
            
            <main>

            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <li data-target="#myCarousel" data-slide-to="5"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="gradient-bx"></div>
                    <div class="item active">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/img_1.jpg" alt="Digital Transformation">
                    </div>
                    <div class="item">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/img_2.jpg" alt="Industries">
                    </div>
                    <div class="item">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/img_3.jpg" alt="Service">
                    </div>
                    <div class="item">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/img_4.jpg" alt="Methodology">
                    </div>
                    <div class="item">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/img_5.jpg" alt="Find Us">
                    </div>
                    <div class="item">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/img_6.jpg" alt="Methodology">
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <div style="border-radius: 50%; width: 100px; height: 100px;">
                        <span class="glyphicon icon-image-left">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/right-arrow.png" class="ar-size" alt="Left Arrow">
                        </span>
                        <span class="sr-only">Previous</span>
                    </div>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon icon-image-right">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/left-arrow.png" class="ar-size" alt="Left Arrow">
                    </span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            
            <?php while (have_posts()) : the_post(); ?>

                <div class="entry-content-page">
                    <?php the_content(); ?> <!-- Page Content -->
                </div>

            <?php endwhile; // End of the loop.
            ?>
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>