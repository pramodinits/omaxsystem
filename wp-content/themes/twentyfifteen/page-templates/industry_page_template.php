<?php
/* Template Name: Industry Page Template */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<title>OMAX</title>
<?php get_header(); ?>
<style>
 .head {
            position: fixed;
            z-index: 99999;
            width: 100%;
            margin: 0px;
            padding: 0px;
            top: 0px;
        }
/*        body {
            overflow-x: hidden !important;
        }*/
        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 900px) {
            .head {
                position: relative;
            }
        }
        /*end*/
        
        #txtName:focus {
            border: 1px solid red;
        }

        #txtEmail:focus {
            border: 1px solid red;
        }

        #txtPhone:focus {
            border: 1px solid red;
        }

        #txtMsg:focus {
            border: 1px solid red;
        }
        #myCarousel p {
            display: none;
        }
</style>
<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <div class="container-fluid head">
            <header class="top_bar-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 logo">
                            <a href="<?php echo home_url(); ?>" title="">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo_MP.png" alt=""/>
                            </a>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3 col-sm-6 toll-free">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/call_header.png"><span>&nbsp;&nbsp;610 707 2244</span>
                            <br/>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mail_header.png"><span>&nbsp;&nbsp;info@solutionprimary.com</span>
                        </div>
                    </div>
                </div>
            </header>
            <nav class="navbar navbar-inverse navbar_ed">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul id="nav" class="nav navbar-nav">
                            <li class=""><a href="<?php echo home_url(); ?>" title="Home">HOME</a></li>
                            <li class="dropdown active">
                                <a href="<?php echo home_url(); ?>/industry-expertise/" class="dropdown-toggle hidden-xs" aria-haspopup="true" aria-expanded="false">INDUSTRY EXPERTISE</a>
                                <a href="#" class="dropdown-toggle hidden-lg hidden-md hidden-sm visible-xs" aria-haspopup="true" aria-expanded="false">INDUSTRY EXPERTISE</a>
                                <ul class="dropdown-menu">
                                    <li class="hidden-lg hidden-md hidden-sm visible-xs"><a href="<?php echo home_url(); ?>/industry-expertise/" title="Industry &amp; Expertise">Industry Expertise</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#finance" title="Financial &amp; Services">Financial &amp; Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#insurance" title="Insurance">Insurance</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#pharmaceuticals" title="Pharmaceuticals">Pharmaceuticals</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#ecommerce" title="E-Commerce">E-Commerce</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#healthcare" title="Health &amp; Care">Health Care</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#media" title="Media">Media</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#telecom" title="Telecom">Telecom</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#hospitality" title="Hospitality">Hospitality</a></li>
                                    <li><a href="<?php echo home_url(); ?>/industry-expertise/#retail" title="Retail">Retail</a></li>
                                </ul>
                            </li>
                            <li id="service" class="dropdown"><a href="#" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">IT SERVICES</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo home_url(); ?>/big-data-analytics/" title="Big Data & Analytics">Big Data & Analytics</a></li>
                                    <li><a href="<?php echo home_url(); ?>/cloud-services/" title="Cloud Services">Cloud Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/it-consulting/" title="IT Consulting">IT Consulting</a></li>
                                    <li><a href="<?php echo home_url(); ?>/software-app-development/" title="Software & App Development">Software & App Development</a></li>
                                    <li><a href="<?php echo home_url(); ?>/testing-services/" title="Testing Services">Testing Services</a></li>
                                    <li><a href="<?php echo home_url(); ?>/it-staffing/" title="IT Staffing">IT Staffing</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo home_url(); ?>/find-jobs/" title="FIND JOBS">FIND JOBS</a></li>
                            <li><a href="<?php echo home_url(); ?>/employers/" title="EMPLOYERS">EMPLOYERS</a></li>
                            <li><a href="<?php echo home_url(); ?>/contact-us/" title="CONTACT US">CONTACT US</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
            
            
            
            <main>
            
            <?php while (have_posts()) : the_post(); ?>

                <div class="entry-content-page">
                    <?php the_content(); ?> <!-- Page Content -->
                </div>

            <?php endwhile; // End of the loop.
            ?>
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer(); ?>